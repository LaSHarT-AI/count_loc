#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-
# =============================================================================
# The MIT License
# 
# Copyright (c) 2020 LaSHarT-AI, (❤ LaSHarT ❤#4564)
# Server Support discord:  NaN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# =============================================================================
"""
La fonction count_loc() est une fonction qui sert à déterminer
le nombre total de ligne de code de votre projet !

Note:
    /!\ La variable ``lines_of_code`` est un object de class str /!\
"""

import subprocess


lines_of_code: str = None

def count_loc():
    """
    Calcule le nombre total de ligne de code total ! (LOC ==> ligne of code)
    """
    global lines_of_code
    try:
        print("Calcule et mise en cache LOC.")

        lines_of_code = subprocess.check_output(
            [
                "/bin/bash",
                "-c",
                # penser à mettre votre nom de dossier ici comme ceci : *dossier_racine*
                "wc -l $(find *NOM_DU_DOSSIER_RACINE* -name '*.py' " 
                '-o -name "*.sql" -o -name '
                '"*.json" -o -name "*.yaml")',
            ], # rajouter l'option '-o -name "*.<extension>"' pour en rajouter au sein de votre projet !
            universal_newlines=True,
        )
        # Obtient le nombres totales de ligne de code !
        lines_of_code = lines_of_code.strip().split("\n")[-1].strip().split(" ")[0]
    finally:
        return


# penser à l'exécuter au moins 1 fois au démarage pour la mise en cache !
count_loc()
print(f"Voici le nombre total de ligne de code : {lines_of_code}")
